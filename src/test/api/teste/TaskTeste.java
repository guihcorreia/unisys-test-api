package test.api.teste;


import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import test.api.modelo.Task;

public class TaskTeste {
	public static void main(String[] args) {
		
		Task task = new Task();
		//task.setId(1);
		task.setName("task 5");
		task.setWeight(6);
		task.setCompleted(true);
		task.setCreatedAt(new Date());
		
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		session.save(task);
		
		session.getTransaction().commit();
		session.close();
	}
	
}
