package test.api.teste;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import test.api.modelo.Job;
import test.api.modelo.Task;

public class JobTeste {
	public static void main(String[] args) {
		
		Task task = new Task();
		task.setName("task 1");
		task.setWeight(6);
		task.setCompleted(true);
		task.setCreatedAt(new Date());
		
		Task task2 = new Task();
		task2.setName("task 2");
		task2.setWeight(2);
		task2.setCompleted(false);
		task2.setCreatedAt(new Date());
		
		Job job = new Job();
		job.setName("job 1");
		job.setActive(true);
		Set<Task> tasks = new HashSet<Task>();
		tasks.add(task);
		tasks.add(task2);
		job.setTasks(tasks);
		
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		session.save(task);
		session.save(task2);
		session.save(job);
		
		session.getTransaction().commit();
		session.close();
	}
}
